#!/bin/sh

set -e
set -u
# DEBUG
set -x

usage() {
        cat <<END
whisper-file.sh usage:
  file="/path/to/soundfile" whisper-file.sh
  or
  whisper-file.sh "/path/to/soundfile"
  if language is not specified, it is auto; possible values: en, es, ca, etc.
  file should not end with, for example, whisper.wav.txt nor whisper.wav
END
}

handle_exit() {
        # handle exit on error
        exit_code="$?"
        if [ ! "${exit_code}" = 0 ]; then
                usage
        fi
}

prepare_f_whisper_arg() {
        # as README.md says, wants 16-bit WAV files
        #   always confirm src https://stackoverflow.com/questions/39788972/ffmpeg-overwrite-output-file-if-exists
        ffmpeg -y -i "${file}" -ar 16000 -ac 1 -c:a pcm_s16le "${f_whisper_arg}"
}

prepare_translate_arg() {
        if [ "${translate}" = "y" ]; then
                translate_arg='--translate'
        else
                translate_arg=''
        fi
}

prepare_output_arg() {
        # is going to catch all output file arguments
        output_arg=''
        old_IFS="${IFS}"
        IFS=','
        for output in ${output_input}; do
                case "${output}" in
                        txt|vtt|srt|lrc|words|csv|json|json-full)
                                f_output="${f_noext}.${output}"
                                if [ ! -f "${f_output}" ] || [ "${force}" = "y" ]; then
                                        output_arg="${output_arg} --output-${output}"
                                else
                                        echo "skipping all processing, file ${f_output} already there"
                                        exit 1
                                fi
                                ;;
                        *)
                                echo 'ERROR: output not supported'
                                exit 1
                                ;;
                esac
        done
        IFS="${old_IFS}"
}

main() {
        trap 'handle_exit' EXIT

        file="${1:-${file}}"

        # I have found very useful to have srt and txt formats by default
        output_input="${output:-txt,srt}"

        # TODO do here a detection of accepted/not accepted audio extensions (.mp4, .mp3, etc.)

        # DEBUG
        #printf "\n\n%s\n" "Considering processing of file ${file}"

        # potential input vars
        ####
        # file
        whisper_path="${whisper_path:-/${HOME}/src/whisper.cpp}"
        whisper_bin="${whisper:-${whisper_path}/main}"
        lang="${lang:-auto}"
        whisper_model=base
        model_path="${model:-${whisper_path}/models/ggml-${whisper_model}.bin}"
        translate="${translate:-n}"

        # processed vars based on input
        ####
        f_noext="${file%.*}"
        f_whisper_arg="/tmp/$(basename ${f_noext})_whisper.wav"

        # if whisper was already processed, don't do it again
        force="${force:-n}"

        prepare_output_arg
        prepare_translate_arg
        prepare_f_whisper_arg

        # whisper command
        ####
        ${whisper_bin} \
                --language ${lang} \
                --model ${model_path} \
                --no-timestamps \
                --file "${f_whisper_arg}" \
                --output-file "${f_noext}" \
                ${output_arg} \
                ${translate_arg}
}

main "${@}"
