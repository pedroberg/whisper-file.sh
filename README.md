# whisper-file.sh

A posix shell helper script to transcript audio files easily using fantastic [whisper.cpp](https://github.com/ggerganov/whisper.cpp) project

You can also translate from any language to English when using `translate=y` environment variable (I perceive the translation is not very good)

I did this mostly for easily process transcription support on audio notes (received from self, or from others)

## Thanks

I first found [whisper.el](https://github.com/natrys/whisper.el/blob/master/whisper.el) project, and from there, I discovered [whisper.cpp](https://github.com/ggerganov/whisper.cpp) project. Congratulations to both projects!

## Installation

Considering a debian.org stable distro.

Requirements:

```
sudo apt install git build-essential
```

Git repository cloning, compilation and model download

```sh
# the directory I dediced to put external git repos, sources, etc. (you can change as you want)
repo_path="${HOME}/src"
mkdir -p "${repo_path}"
cd "${repo_path}"
git clone https://github.com/ggerganov/whisper.cpp
cd whisper.cpp
# 2024-01-13_18-24-07, on this date, that works for me
git checkout v1.5.1
# instructions from https://github.com/ggerganov/whisper.cpp/tree/master#quick-start
#   download "base.en" whisper model
bash ./models/download-ggml-model.sh base.en
#   compile
make

cd base && models/download-ggml-model.sh base
```

Then add the script to your local PATH, I use `~/bin`, so I placed there the `whisper-file.sh` in order to use it

I usually put something like this in my `~/.bashrc` file:

```
add_path() {
        mypath="${1}"
        if [ -d "${mypath}" ]; then
                if echo ":${PATH}:" | grep -qv ":${mypath}:"; then
                        PATH="${PATH}:${mypath}"
                fi
        fi
}

add_path "/home/$USER/bin"
```

## Usage and what it does

The default configuration automatically detects the main language

```sh
whisper-file.sh msg12312456.ogg
```

File is (and will be) the only supported arg, but you can have a lot of options like this (or `source` them from an env file).

In the following example, translation to English is done, it forces regenerating file, and we skip language autodetection (or we force to say the file is in catalan)

```sh
translate=y force=y lang=ca file=msg12312456.ogg whisper-file.sh
```

Given a file such as *msg12312456.ogg*, it converts it to *msg12312456.wav* (in the way whisper wants, I looked at examples of the reference repo) and then put it on a text file *msg12312456_whisper.wav.txt*.

If you do it again to the file, it skips the file (which is useful to process on a inbox directory of audio notes or downloaded notes)

## Room for improvement

https://github.com/ggerganov/whisper.cpp/tree/master/models#fine-tuned-models

## About the whisper models

Here you have more info on what models to use

https://github.com/ggerganov/whisper.cpp/tree/master/models

I briefly tried:

- `model.en` did not worked when I tried to translate from catalan to english *(speaking in foreign language) (speaking in foreign language) (speaking in foreign language)*
- `large-v3` but it is very slow in my laptop, it did not even finish after a minute
